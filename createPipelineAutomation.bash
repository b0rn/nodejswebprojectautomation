#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

read -p "What's the project name ? " projectName

read -p "Do you want to configure a git repo with dev and prod branches (with automatic deployment) ? " -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	/bin/bash configGit.bash $projectName
fi
echo

read -p "Do you want to configure nginx ? " -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	/bin/bash configNginx.bash $projectName
fi
echo

echo "All done."

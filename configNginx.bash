#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ $# -eq 0 ]; then
	read -p "What is the project name ? " projectName
else
	projectName=$1
fi

echo "Ports already in use : "
for filename in /etc/nginx/conf.d/*; do
	name=`sed '1q;d' $filename | cut -d " " -f 2`
	port=`sed '2q;d' $filename | cut -d " " -f 2 | cut -d ":" -f 2`
	port=${port::-1}
	echo "$name --> $port"
done

read -p "What is the domain name ? " domainName

createNginxConf() {
	local url="$projectName.$domainName"
	if [ $1 == "dev" ]; then
		url="dev.$projectName.$domainName"
	fi

	read -p "Which port do you want to run $url on ? : " projectPort


	local filePath="/etc/nginx/conf.d/www.$url.conf"

	echo "Creating config file : $filePath"
	cd $DIR
	cp conf.d/nginxConf.conf $filePath
	sed -i -e "s/\${PROJECT}/$projectName/g" $filePath
	sed -i -e "s/\${ENV}/$1/g" $filePath
	sed -i -e "s/\${PORT}/$projectPort/g" $filePath
	sed -i -e "s/\${URL}/$url/g" $filePath

	read -p "Do you want to add an SSL certificate to this configuration ? (y/n) " -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		certbot --nginx -d $url -d "www.$url"
	fi

	read -p "Complete project env files ? (y/n) " -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		local BRANCH="master"
		if [ $1 == "dev" ]; then
			$BRANCH="dev"
		fi
		local file="/srv/${projectName}/env/${projectName}_$BRANCH.env"
		sed -i -e "s/\${PORT}/$projectPort/g" $file
		sed -i -e "s/\${DOMAIN}/www.$url/g" $file
	fi

	echo "Done configuring $url and www.$url"
}

read -p "Create dev environment ? (y/n) " -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	createNginxConf "dev"
fi

read -p "Cread prod environment ? (y/n) " -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	createNginxConf "prod"
fi

echo "Done configuring nginx."

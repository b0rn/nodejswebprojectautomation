#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ $# -eq 0 ]; then
	read -p 'Project name : ' projectName
else
	projectName=$1
fi

createAndGrant(){
	echo "Creating $1"
	mkdir -p $1
	echo "Granting permissions to group 'users' in $1"
	chgrp -R users $1
	chmod g+w $1
	echo
}

createRandomPassphrase(){
	if [ -z $1 ];then
		$1=35
	fi
	/dev/urandom tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' | head -c $1; echo
}

buildBranchEnvFile(){
	if [ -z $1 ];then
		echo "No argument given to buildBranchEnvFile !"
		return;
	fi
	local file="/srv/${projectName}/env/${projectName}_$1.env"
	local LOGS_DIR="/srv/$projectName/logs/$1"
	sed -i -e "s/\${LOGS_DIR}/$LOGS_DIR/g" $file
	sed -i -e "s/\${COOKIE_SECRET}/$(createRandomPassphrase 35)/g" $file
}



createAndGrant "/srv/tmp"
createAndGrant "/srv/www"
createAndGrant "/srv/git"
createAndGrant "/srv/$projectName"
createAndGrant "/srv/$projectName/env"
createAndGrant "/srv/$projectName/logs"
createAndGrant "/srv/$projectName/logs/dev"
createAndGrant "/srv/$projectName/logs/master"
createAndGrant "/srv/$projectName/static_files"
createAndGrant "/srv/$projectName/node_modules"
createAndGrant "/srv/$projectName/node_modules/dev"
createAndGrant "/srv/$projectName/node_modules/master"

echo "Creating /srv/git/$projectName.git/"
mkdir -p "/srv/git/$projectName.git/"
echo "Initializing git repo..."
git init --bare "/srv/git/$projectName.git/"
echo "Granting permissions to group 'users' in /srv/git/$projectName.git/"
chgrp -R users "/srv/git/$projectName.git/"
chmod -R g+rwX "/srv/git/$projectName.git/"
find "/srv/git/$projectName.git/" -type d -exec chmod g+s '{}' +

echo "Copying automation files..."
cp conf.d/post-receive "/srv/git/$projectName.git/hooks/"
cp conf.d/deployApp.bash "/srv/$projectName/"
sed -i -e "s/\${PROJECT}/$projectName/g" "/srv/git/$projectName.git/hooks/post-receive"
sed -i -e "s/\${PROJECT}/$projectName/g" "/srv/$projectName/deployApp.bash"

echo "Building env files..."
cp conf.d/global.env "/srv/$projectName/env/global.env"
cp conf.d/project_global.env "/srv/$projectName/env/${projectName}_global.env"
cp conf.d/project_branch.env "/srv/$projectName/env/${projectName}_dev.env"
cp conf.d/project_branch.env "/srv/$projectName/env/${projectName}_master.env"
sed -i -e "s/\${PROJECT}/$projectName/g" "/srv/${projectName}/env/${projectName}_global.env"
buildBranchEnvFile "master"
buildBranchEnvFile "dev"


echo "Setting git conf..."
cd "/srv/git/$projectName.git/"
git config core.sharedRepository group
git config receive.advertisePushOptions true

echo "Done configuring git."
echo "Don't forget to edit your env files ;)"

#!/bin/bash

if [ "$1" == "" ]; then
	echo "Nothing to deploy."
else
	GIT_REPO="/srv/git/${PROJECT}.git"
	GLOBAL_ENV_FILE="/srv/${PROJECT}/env/${PROJECT}_global.env"
	STATIC_FILES="/srv/${PROJECT}/static_files"
	DEPLOY=false
	FULL_AUDIT=false
	PRODUCTION=false

	if [ "$2" == "full-audit" ]; then
		FULL_AUDIT=true
	fi

	if [ "$1" == "dev" ]; then
		GIT_TARGET="/srv/www/${PROJECT}/dev"
		GIT_TMP="/srv/tmp/${PROJECT}/dev"
		ENV_FILE="/srv/${PROJECT}/env/${PROJECT}_dev.env"
		NODE_MODULES="/srv/${PROJECT}/node_modules/dev"
		DEPLOY=true
	elif [ "$1" == "master" ]; then
		GIT_TARGET="/srv/www/${PROJECT}/master"
		GIT_TMP="/srv/tmp/${PROJECT}/master"
		ENV_FILE="/srv/${PROJECT}/env/${PROJECT}_master.env"
		NODE_MODULES="/srv/${PROJECT}/node_modules/master"
		DEPLOY=true
		PRODUCTION=true
	fi

	if [ $DEPLOY == true ]; then
		#Move files to TMP
		mkdir -p $GIT_TMP
		git --work-tree=$GIT_TMP --git-dir=$GIT_REPO checkout $1 -f

		#Create symbolic links
		ln -s "$NODE_MODULES/node_modules" "$GIT_TMP/node_modules"
		ln -s $STATIC_FILES "$GIT_TMP/dist/${PROJECT}/static_files"

		#Install node modules
		echo "Installing node modules..."
		cp "$GIT_TMP/"{package.lock,package-lock.json} $NODE_MODULES
		cd $GIT_TMP
		if [ PRODUCTION == true ]; then
			npm install --production --no-audit > /dev/null 2>&1
		else
			npm install --no-audit > /dev/null 2>&1
		fi

		#Audit node modules
		echo "Performing basic node modules audit..."
		npm audit
		if [ $FULL_AUDIT == true ]; then
			echo "Performing full node modules audit..."
			snyk test && snyk monitor
		fi

		#Copy env files
		echo "Copying env files..."
		cp $ENV_FILE "$GIT_TMP/.env"
		cat $GLOBAL_ENV_FILE >> "$GIT_TMP/.env"

		#Move TMP to TARGET
		cd /
		rm -rf $GIT_TARGET
		mv $GIT_TMP $GIT_TARGET

		#Give rights to TARGET
		chmod 755 $GIT_TARGET

		#Deploy the app
		cd $GIT_TARGET
		echo "Deploying the app..."

		source /srv/${PROJECT}/env/global.env
		( (pm2 delete "${PROJECT}_$1-githook" || true) && pm2 start app.js --name "${PROJECT}_$1-githook" ) > /dev/null 2>&1

		echo "App deployed ($1)"
	fi
fi
